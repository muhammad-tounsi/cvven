/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tounsi
 */
public class Salle {
    private int num_salle;
    private String equipement, type_salle;
    private int nb_places_max;
    
    public Salle(){
        
    }

    public Salle(int num_salle, String equipement, String type_salle, int nb_places_max) {
        this.num_salle = num_salle;
        this.equipement = equipement;
        this.type_salle = type_salle;
        this.nb_places_max = nb_places_max;
    }
    
    public Salle(String equipement, String type_salle, int nb_places_max) {
        this.equipement = equipement;
        this.type_salle = type_salle;
        this.nb_places_max = nb_places_max;
    }

    public int getNum_salle() {
        return num_salle;
    }

    public void setNum_salle(int num_salle) {
        this.num_salle = num_salle;
    }

    public String getEquipement() {
        return equipement;
    }

    public void setEquipement(String equipement) {
        this.equipement = equipement;
    }

    public String getType_salle() {
        return type_salle;
    }

    public void setType_salle(String type_salle) {
        this.type_salle = type_salle;
    }

    public int getNb_places_max() {
        return nb_places_max;
    }

    public void setNb_places_max(int nb_places_max) {
        this.nb_places_max = nb_places_max;
    }

    @Override
    public String toString() {
        return "Salle{" + "num_salle=" + num_salle + ", equipement=" + equipement + ", type_salle=" + type_salle + ", nb_places_max=" + nb_places_max + '}';
    }
    
    
}
