/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Muhammad
 */
public class ITafficherParticipant extends javax.swing.JFrame {
    private DAO<Participant> pa = new ParticipantDAO();
    private ArrayList<Participant> lesParticipants;

    /**
     * Creates new form ITafficherParticipant
     */
    public ITafficherParticipant() throws SQLException {
        initComponents();
        lesParticipants = new ArrayList<Participant>();
        lesParticipants = pa.findAll();
        cbPart.removeAllItems();
        cbPart.addItem("Aucun");
        for(int i = 0 ; i < lesParticipants.size() ; i++){
            System.out.println(lesParticipants.get(0).getNom());
            cbPart.addItem(lesParticipants.get(i).getNom());   
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        cbPart = new javax.swing.JComboBox<String>();
        jSeparator2 = new javax.swing.JSeparator();
        txtNom = new javax.swing.JLabel();
        txtPrenom = new javax.swing.JLabel();
        txtEmail = new javax.swing.JLabel();
        txtDateNaiss = new javax.swing.JLabel();
        txtOrganisation = new javax.swing.JLabel();
        txtObservation = new javax.swing.JLabel();
        txtNom1 = new javax.swing.JLabel();
        txtPrenom1 = new javax.swing.JLabel();
        txtEmail1 = new javax.swing.JLabel();
        txtDateNaiss1 = new javax.swing.JLabel();
        txtOrganisation1 = new javax.swing.JLabel();
        txtObservation1 = new javax.swing.JLabel();
        btnAccueil = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        btnSupprimer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Liste des participants");

        jLabel2.setText("Nom des participants :");

        cbPart.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbPart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPartActionPerformed(evt);
            }
        });

        txtNom.setText("Nom : ");

        txtPrenom.setText("Prénom :");

        txtEmail.setText("email :");

        txtDateNaiss.setText("Date de naissance :");

        txtOrganisation.setText("Organisation :");

        txtObservation.setText("Observations :");

        txtNom1.setText("Nom :");

        txtPrenom1.setText("Prénom :");

        txtEmail1.setText("email :");

        txtDateNaiss1.setText("Date de naissance :");

        txtOrganisation1.setText("Organisation :");

        txtObservation1.setText("Observations :");

        btnAccueil.setText("Retourner à l'accueil");
        btnAccueil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccueilActionPerformed(evt);
            }
        });

        btnSupprimer.setText("Supprimer");
        btnSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSupprimerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(txtNom))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPrenom, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtDateNaiss, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtOrganisation, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtObservation, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPrenom1)
                    .addComponent(txtNom1)
                    .addComponent(txtEmail1)
                    .addComponent(txtDateNaiss1)
                    .addComponent(txtOrganisation1)
                    .addComponent(txtObservation1))
                .addGap(164, 164, 164))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAccueil, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(256, 256, 256)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(59, 59, 59)
                                .addComponent(cbPart, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 618, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(248, 248, 248)
                        .addComponent(btnSupprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbPart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNom)
                    .addComponent(txtNom1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrenom)
                    .addComponent(txtPrenom1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEmail)
                    .addComponent(txtEmail1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDateNaiss)
                    .addComponent(txtDateNaiss1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOrganisation)
                    .addComponent(txtOrganisation1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtObservation)
                    .addComponent(txtObservation1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(btnSupprimer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccueil)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbPartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPartActionPerformed
        // TODO add your handling code here:
        String choix = (String)cbPart.getSelectedItem();
        if(choix != null){
            if(choix.equals("Aucun")){
                txtNom1.setText("");
                txtPrenom1.setText("");
                txtEmail1.setText("");
                txtDateNaiss1.setText("");
                txtOrganisation1.setText("");
                txtObservation1.setText("");
            }
            else{
                for(int i = 0 ; i < lesParticipants.size() ; i++){
                    if(lesParticipants.get(i).getNom().equals(choix)){
                        txtDateNaiss1.setText(lesParticipants.get(i).getDate_naiss());
                        txtNom1.setText(lesParticipants.get(i).getNom());
                        txtPrenom1.setText(lesParticipants.get(i).getPrenom());
                        txtEmail1.setText(lesParticipants.get(i).getEmail());
                        txtOrganisation1.setText(lesParticipants.get(i).getOrganisation());
                        txtObservation1.setText(lesParticipants.get(i).getObservations()); 
                    }
                    }
            }            
        }
    }//GEN-LAST:event_cbPartActionPerformed

    private void btnAccueilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccueilActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new ITaccueil().setVisible(true);
    }//GEN-LAST:event_btnAccueilActionPerformed

    private void btnSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSupprimerActionPerformed
        // TODO add your handling code here:
        int choix = cbPart.getSelectedIndex() - 1;
        Participant lp = lesParticipants.get(choix);
        pa.delete(lp);
        //JOptionPane.showMessageDialog(null, "L'événement a été supprimé !", "Suppression", JOptionPane.INFORMATION_MESSAGE);
        this.dispose();
        try {
            new ITafficherParticipant().setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(ITafficherParticipant.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSupprimerActionPerformed

  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccueil;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JComboBox<String> cbPart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel txtDateNaiss;
    private javax.swing.JLabel txtDateNaiss1;
    private javax.swing.JLabel txtEmail;
    private javax.swing.JLabel txtEmail1;
    private javax.swing.JLabel txtNom;
    private javax.swing.JLabel txtNom1;
    private javax.swing.JLabel txtObservation;
    private javax.swing.JLabel txtObservation1;
    private javax.swing.JLabel txtOrganisation;
    private javax.swing.JLabel txtOrganisation1;
    private javax.swing.JLabel txtPrenom;
    private javax.swing.JLabel txtPrenom1;
    // End of variables declaration//GEN-END:variables
}
