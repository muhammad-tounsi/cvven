/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tounsi
 */

package cvven;

import cvven.Participant;
import cvven.DAO;
import cvven.ITparticipant;
import cvven.Participant;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ParticipantDAO extends DAO<Participant> {
    
    public Participant find(int id){
        Participant p = new Participant();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM participant WHERE num_pers = " + id);
            if(result.first()){
                p = new Participant(id,result.getString("nom"),result.getString("prenom"), result.getString("email"),
                        result.getString("date_naiss"), result.getString("organisation"), result.getString("observations"));
                }} catch (SQLException e) {
                        e.printStackTrace();
                        }
                return p;
                }
    
    public Participant create(Participant p){
        String nom, prenom, email, date_naiss, organisation, observations;
        nom = p.getNom();
        System.out.println(p.getNom());
        prenom = p.getPrenom();
        date_naiss = p.getDate_naiss();
        email = p.getEmail();
        organisation = p.getOrganisation();
        observations = p.getObservations();
        JOptionPane Jop = new JOptionPane();

        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO participant (nom, prenom, date_naiss, email, organisation, observation)"
                    + "VALUES ('" + nom + "', '" + prenom + "', '" + date_naiss + "','" + email + "','" + organisation + "','" + observations + "')");
            prepare.executeUpdate();
            Jop.showMessageDialog(null, "La personne a bien été ajoutée", "Validation", JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException e) {
            
            Jop.showMessageDialog(null, "Les champs doivent être remplies !", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        return p;
    }
        
    
    public Participant update(Participant p){
        int num_pers = p.getNum_pers();
        String nom, prenom, email, date_naiss, organisation, observations;
        nom = p.getNom();
        prenom = p.getPrenom();
        date_naiss = p.getDate_naiss();
        email = p.getEmail();
        organisation = p.getOrganisation();
        observations = p.getObservations();

        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE participant SET nom='" + nom + "', prenom='" + prenom + "', date_naiss='" + date_naiss + "', email='" + email + "', organisation='" + organisation + "', observation='" + observations + "' WHERE num_pers='" + num_pers + "' ");
            prepare.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return p;
    }
    
    public void delete(Participant p){
        int num_pers = p.getNum_pers();
        JOptionPane Jop = new JOptionPane();
        
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM participant WHERE num_pers='" + num_pers + "'");
            prepare.executeUpdate();
            Jop.showMessageDialog(null, "La personne a bien été supprimée", "Validation", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
    public ArrayList<Participant> findAll() throws SQLException{
        ArrayList<Participant> lesParticipants = new ArrayList<Participant>();
        Participant pa = new Participant();
        ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM participant");
        while(result.next()){
            pa = new Participant(result.getInt("num_pers"), result.getString("nom"),result.getString("prenom"), result.getString("date_naiss"),
                        result.getString("email"), result.getString("organisation"), result.getString("observation"));
            lesParticipants.add(pa);
        }
        return lesParticipants;
    }
    
    public ArrayList<Participant> findParticipantAEvenement(int num_even) {
        ArrayList<Participant> lesParticipants = new ArrayList<Participant>();
        try {
            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).
            executeQuery("SELECT * FROM participant,inscrire WHERE participant.num_pers = inscrire.fk_num_pers AND inscrire.fk_num_even = '" + num_even +"'");
            while(result.next()){
                int nump = result.getInt("num_pers");
                String nom = result.getString("nom");
                String prenom = result.getString("prenom");
                String dateNaiss = result.getString("date_naiss");
                String mail = result.getString("email");
                String organisation = result.getString("organisation");
                String observations = result.getString("observation");
                lesParticipants.add(new Participant(nump,nom,prenom,dateNaiss,mail,organisation,observations));
            }
        }
        catch (SQLException e) {
             e.printStackTrace();
        }
        return lesParticipants;
  }
}
    
    

