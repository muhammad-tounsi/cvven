package cvven;
import java.sql.Connection;
import java.sql.DriverManager; 
import java.sql.ResultSet; 
import java.sql.Statement;
import java.sql.SQLException;
public class Connexion 
{
        //private static String url = "jdbc:postgresql://srv-ent/groupe2CJAVA";
        //private static String url = "jdbc:postgresql://rene-descartes.hd.free.fr/TounsiJava";
        private static String url = "jdbc:postgresql://rene-descartes.hd.free.fr/groupe2CJAVA";
        private static String user = "groupe2c";
        private static String pwd = "mdp2c";
        
        private static Connection connect;
        
        
    
    public Connexion() {

    }
    
    public static Connection getInstance(){
        if(connect == null){
            try{
                connect = DriverManager.getConnection(url, user, pwd);
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return connect;
    }
}
