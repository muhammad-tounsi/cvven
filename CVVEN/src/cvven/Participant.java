/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

/**
 *
 * @author tounsi
 */
public class Participant {
    
    private int num_pers = 0;
    private String nom = "";
    private String prenom = "";
    private String email = "";
    private String date_naiss;
    private String organisation = "";
    private String observations = "";

    public Participant() {
    }

    public Participant(int num_pers, String nom, String prenom, String date_naiss, String email, String organisation, String observations) {
        this.num_pers = num_pers;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.date_naiss = date_naiss;
        this.organisation = organisation;
        this.observations = observations;
    }
    
    public Participant(String nom, String prenom, String date_naiss, String email, String organisation, String observations) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.date_naiss = date_naiss;
        this.organisation = organisation;
        this.observations = observations;
    }
    
    public int getNum_pers() {
        return num_pers;
    }

    public void setNum_pers(int num_pers) {
        this.num_pers = num_pers;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate_naiss() {
        return date_naiss;
    }

    public void setDate_naiss(String date_naiss) {
        this.date_naiss = date_naiss;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String toString() {
        return "Personne [num_pers=" + num_pers + ", nom=" + nom + ", prenom="
                + prenom + ", email=" + email + ", date_naiss=" + date_naiss
                + ", organisation=" + organisation + ", observations="
                + observations + "]";
    }

}

