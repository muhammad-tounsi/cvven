/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.sql.SQLException;


/**
 *
 * @author Muhammad
 */
public class InscrireDAO extends DAO<Inscrire> {
    
    public Inscrire find(int id) {
        Inscrire i = new Inscrire();
        try {
            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM inscrire WHERE fk_num_pers = " + id);
            if(result.first()){
                i = new Inscrire(id,result.getInt("fk_num_even"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    public Inscrire create(Inscrire i) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO inscrire(fk_num_pers, fk_num_even)"
                    + " VALUES(" +i.getFk_num_pers() + ", " +i.getFk_num_even()+ ")");
            prepare.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    public Inscrire update(Inscrire i) {
        int num_pers = i.getFk_num_pers();
        int num_even = i.getFk_num_even();
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE inscrire SET fk_num_pers='" + num_pers + "', fk_num_even='" + num_even + "'");
            prepare.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    public void delete(Inscrire i) {
        int num_even = i.getFk_num_even();
        int num_pers = i.getFk_num_pers();

        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM inscrire WHERE fk_num_pers='" + num_pers + "' AND fk_num_even='"+ num_even +"'");
            prepare.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Inscrire> findAll() throws SQLException {
        ArrayList<Inscrire> lesInscriptions= new ArrayList<Inscrire>();
        Inscrire ins = new Inscrire();
        try {
            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM inscrire");
            while(result.next()){
                ins = new Inscrire(result.getInt("fk_num_pers"), result.getInt("fk_num_even"));
                lesInscriptions.add(ins);
            }
        }
        catch (SQLException e) {
             e.printStackTrace();
         }
        return lesInscriptions;
    }
    
    /*public ArrayList<Participant> findParticipantAEvenement(int num_even) {
        ArrayList<Participant> lesParticipants = new ArrayList<Participant>();
        try {
            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).
            executeQuery("SELECT * FROM participant,inscrire WHERE participant.num_pers = inscrire.fk_num_pers AND inscrire.fk_num_even = '" + num_even +"'");
            while(result.next()){
                int nump = result.getInt("num_pers");
                String nom = result.getString("nom");
                String prenom = result.getString("prenom");
                String dateNaiss = result.getString("date_naiss");
                String mail = result.getString("email");
                String organisation = result.getString("organisation");
                String observations = result.getString("observation");
                lesParticipants.add(new Participant(nump,nom,prenom,dateNaiss,mail,organisation,observations));
            }
        }
        catch (SQLException e) {
             e.printStackTrace();
        }
        return lesParticipants;
  }*/
      
}

