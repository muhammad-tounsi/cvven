package cvven;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tounsi
 */
import cvven.Evenement;
import cvven.DAO;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import java.util.ArrayList;


public class EvenementDAO extends DAO<Evenement> {
    
    public Evenement find(int id){
        Evenement ev = new Evenement();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM evenement WHERE num_even = " + id);
            if(result.first()){
                ev = new Evenement(id,result.getString("intitule"),result.getInt("fk_type_even"), result.getString("description"),
                        result.getString("organisateur"), result.getString("date_debut"), result.getString("date_fin"), result.getInt("nb_part_max"), result.getInt("fk_num_salle"));
            }
        }   catch (SQLException e) {
                e.printStackTrace();
            }
        return ev;
    }
    
    @Override
    public ArrayList<Evenement> findAll() throws SQLException{
        ArrayList<Evenement> lesTypes = new ArrayList<Evenement>();
        Evenement te = new Evenement();
        ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM evenement");
        while(result.next()){
            te = new Evenement(result.getInt("num_even"), result.getString("intitule"),result.getInt("fk_type_even"), result.getString("description"),
                        result.getString("organisateur"), result.getString("date_debut"), result.getString("date_fin"), result.getInt("nb_part_max"), result.getInt("fk_num_salle"));
            lesTypes.add(te);
        }
        return lesTypes;
    }
/* Manque ici les méthodes create, update et delete */

    @Override
    public Evenement create(Evenement ev) {
        int nb_part_max; 
        int theme;
        int salle;
        String intitule, description, organisateur, date_debut, date_fin;
        intitule = ev.getIntitule();
        theme = ev.getTheme();
        description = ev.getDescription();
        organisateur = ev.getOrganisateur();
        date_debut = ev.getDate_debut();
        date_fin = ev.getDate_fin();
        nb_part_max = ev.getNb_part_max();
        salle = ev.getSalle();
        JOptionPane Jop = new JOptionPane();

        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO evenement(intitule, date_debut, date_fin, nb_part_max, description, organisateur, fk_type_even, fk_num_salle) "
                    + "VALUES('" + intitule + "', '"   + date_debut + "','" + date_fin + "','" + nb_part_max + "', '" + description + "','" + organisateur + "','" + theme + "','"+ salle+"')");
            prepare.executeUpdate();
            Jop.showMessageDialog(null, "L'Evenement a bien été ajoutée", "Validation", JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException e) {
            e.printStackTrace();
            Jop.showMessageDialog(null, "Les champs doivent être remplies !", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        return ev;
    }
    

    @Override
    public Evenement update(Evenement ev) {
     int num_even = ev.getNum_even();
     int theme;
     int nb_part_max;
     int salle;
        String intitule, description, organisateur, date_debut, date_fin;
        intitule = ev.getIntitule();
        theme = ev.getTheme();
        description = ev.getDescription();
        organisateur = ev.getOrganisateur();
        date_debut = ev.getDate_debut();
        date_fin = ev.getDate_fin();
        nb_part_max = ev.getNb_part_max();
        salle = ev.getSalle();

        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE evenement SET intitule='"+ intitule +"',fk_type_event='" + theme + "' description='" + description + "', organisateur='" + organisateur + "', date_debut='" + date_debut + "', date_fin='" + date_fin + "', nb_part_max='" + nb_part_max + "', fk_num_salle='"+ salle+ "' WHERE num_even='" + num_even + "' ");
            prepare.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ev;
    }
    

    @Override
    public void delete(Evenement ev) {
        int num_even = ev.getNum_even();
        JOptionPane Jop = new JOptionPane();
        
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM evenement WHERE num_even='" + num_even + "'");
            prepare.executeUpdate();
            Jop.showMessageDialog(null, "L'Evenement a bien été supprimé", "Validation", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}