/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

/**
 *
 * @author Muhammad
 */
public class Inscrire {
    
     private int fk_num_pers, fk_num_even;

    public Inscrire() {
    }

    public Inscrire(int fk_num_pers, int fk_num_even) {
        this.fk_num_pers = fk_num_pers;
        this.fk_num_even = fk_num_even;
    }

    public int getFk_num_pers() {
        return fk_num_pers;
    }

    public void setFk_num_pers(int fk_num_pers) {
        this.fk_num_pers = fk_num_pers;
    }

    public int getFk_num_even() {
        return fk_num_even;
    }
    
    public void setFk_num_even(int fk_num_even) {
        this.fk_num_even = fk_num_even;
    }

    public String toString() {
        return "Inscrire [fk_num_pers=" + fk_num_pers + ", fk_num_even="
                + fk_num_even + "]";
    }
}
