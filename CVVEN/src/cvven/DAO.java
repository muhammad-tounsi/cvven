package cvven;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tounsi
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DAO<T> {
    public Connection connect = Connexion.getInstance();
    /*Trouver une entrée dans la base à l'aide de son id*/
    public abstract T find(int id);
    
    public ArrayList<T> findAll() throws SQLException{
        return null;
    }
    /*Créer une entrée dans la base à partir d'un objet */
    public abstract T create(T obj);
    /*Mettre à jour les données d'une entrée dans la base */
    public abstract T update(T obj);
    /*Suppression d'une entrée de la base */
    public abstract void delete(T obj);
    
    public ArrayList<T> findParticipantAEvenement(int num_even){
        return null;
    }
}
