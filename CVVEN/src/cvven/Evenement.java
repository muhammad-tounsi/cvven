package cvven;

/**
 *
 * @author tounsi
 */
public class Evenement {
    private int num_even = 0;
    private String intitule, description, organisateur,
            date_debut, date_fin;
    private int nb_part_max, theme, salle;
    
    public Evenement(){
        
    }


    public Evenement(String intitule, int theme, String description, String organisateur, String date_debut, String date_fin, int nb_part_max, int salle) {
        this.intitule = intitule;
        this.theme = theme;
        this.description = description;
        this.organisateur = organisateur;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.nb_part_max = nb_part_max;
        this.salle = salle;
    }
    

     public Evenement(int num_even, String intitule, int theme, String description, String organisateur, String date_debut, String date_fin, int nb_part_max, int salle) {
        this.num_even = num_even;
        this.intitule = intitule;
        this.theme = theme;
        this.description = description;
        this.organisateur = organisateur;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.nb_part_max = nb_part_max;
        this.salle = salle;
    }

    public int getNum_even() {
        return num_even;
    }

    public void setNum_even(int num_even) {
        this.num_even = num_even;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }
        
    public String getDescription() {
        return description;
    }
    
    public int getTheme(){
        return theme;
    }
    
    public void setTheme(int theme){
        this.theme = theme;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public int getNb_part_max() {
        return nb_part_max;
    }

    public void setNb_part_max(int nb_part_max) {
        this.nb_part_max = nb_part_max;
    }
    
    public int getSalle(){
        return salle;
    }
    
    public void setSalle(int salle){
        this.salle = salle;
    }

    @Override
    public String toString() {
        return "Evenement{" + "num_even=" + num_even + ", intitule=" + intitule + ", description=" + description + ", organisateur=" + organisateur + ", date_debut=" + date_debut + ", date_fin=" + date_fin + ", nb_part_max=" + nb_part_max + '}';
    }
     
     
    
}
