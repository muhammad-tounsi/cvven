/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mat
 */
public class typeEvenementDAO extends DAO<typeEvenement>{

    @Override
    public typeEvenement find(int id) {
        typeEvenement ev = new typeEvenement();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM type_evenement WHERE type_even = " + id);
            if(result.first()){
                ev = new typeEvenement(result.getInt("type_even"),result.getString("nom_theme"));
            }
        }   catch (SQLException e) {
                e.printStackTrace();
            }
        return ev;
    }

    @Override
    public typeEvenement create(typeEvenement obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public typeEvenement update(typeEvenement obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(typeEvenement obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public ArrayList<typeEvenement> findAll() throws SQLException{
        ArrayList<typeEvenement> lesTypes = new ArrayList<typeEvenement>();
        typeEvenement te = new typeEvenement();
        try {
        ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM type_evenement");
        while(result.next()){
            te = new typeEvenement(result.getInt("type_evenement"), result.getString("nom_theme"));
            lesTypes.add(te);
        }
        } catch (SQLException e) {
                e.printStackTrace();
            }
        return lesTypes;
    }
}
