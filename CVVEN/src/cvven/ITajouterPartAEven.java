/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Muhammad
 */
public class ITajouterPartAEven extends javax.swing.JFrame {

    /**
     * Creates new form ITajouterPartAEven
     */
    
    JOptionPane Jop = new JOptionPane();
    ArrayList<Evenement> lesEvenements = new ArrayList<Evenement>();
    //EvenementDAO evenement = new EvenementDAO();
    DAO<Evenement> evenement = new EvenementDAO();
    ArrayList<Participant> lesParticipants = new ArrayList<Participant>();
    //ParticipantDAO participant = new ParticipantDAO();
    DAO<Participant> participant = new ParticipantDAO();
    //InscrireDAO inscrire = new InscrireDAO();
    DAO<Inscrire> inscrire = new InscrireDAO();
    ArrayList<Participant> lesParticipantsDeLEvenement = new ArrayList<Participant>();
    
    public ITajouterPartAEven() throws SQLException {
        initComponents();
        lesParticipants = participant.findAll();
        for (Participant unParticipant : lesParticipants) {
            cbPart.addItem(unParticipant.getNom());
            System.out.println(unParticipant.getNum_pers());
            //cbPart1.addItem(unParticipant.getNom());
        }
        lesEvenements = evenement.findAll();
        for (Evenement unEvenement : lesEvenements) {
            cbEven.addItem(unEvenement.getIntitule());
            cbEven1.addItem(unEvenement.getIntitule());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnAjout = new javax.swing.JButton();
        cbPart = new javax.swing.JComboBox<String>();
        cbEven = new javax.swing.JComboBox<String>();
        jSeparator2 = new javax.swing.JSeparator();
        btnAccueil = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cbEven1 = new javax.swing.JComboBox<String>();
        cbPart1 = new javax.swing.JComboBox<String>();
        btnRetirer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("Gestion des participants aux événements");

        jLabel2.setText("Ajouter le participant : ");

        jLabel3.setText("A l'événement :");

        btnAjout.setText("Ajouter le participant à l'événement");
        btnAjout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjoutActionPerformed(evt);
            }
        });

        btnAccueil.setText("Retourner à l'accueil");
        btnAccueil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccueilActionPerformed(evt);
            }
        });

        jLabel4.setText("Ajout d'un participant à un événement");

        jLabel5.setText("Retirer d'un événement un participant");

        jLabel6.setText("Dans l'événement :");

        jLabel7.setText("Retirer le participant :");

        cbEven1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEven1ActionPerformed(evt);
            }
        });

        btnRetirer.setText("Retirer le participant de l'événement");
        btnRetirer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetirerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAccueil)
                .addGap(193, 193, 193))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(92, 92, 92))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2)
                            .addComponent(jSeparator1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbPart, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbEven, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(cbEven1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbPart1, 0, 138, Short.MAX_VALUE)))
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(btnAjout))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(btnRetirer)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(cbPart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbEven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(btnAjout)
                .addGap(18, 18, 18)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(cbEven1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbPart1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(btnRetirer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccueil)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAjoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjoutActionPerformed
        
        // on crée l'objet en passant en paramétre une chaîne representant le format 
        /*SimpleDateFormat formatter = new SimpleDateFormat ("yyyy.MM.dd" ); 
        //récupération de la date courante 
        Date currentTime_1 = new Date(); 
        //on crée la chaîne à partir de la date  
        String dateBonFormat = formatter.format(currentTime_1);
        */
        int index1,index2;
        index1=cbPart.getSelectedIndex();
        index2=cbEven.getSelectedIndex();
        System.out.println(index1);
        System.out.println(lesParticipants.get(2).getNum_pers());
        //lesParticipants.
        inscrire.create(new Inscrire(lesParticipants.get(index1).getNum_pers(),lesEvenements.get(index2).getNum_even()));
        Jop.showMessageDialog(null, "La personne a été ajoutée à l'événement", "Validation", JOptionPane.INFORMATION_MESSAGE);
        
    }//GEN-LAST:event_btnAjoutActionPerformed

    private void btnAccueilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccueilActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new ITaccueil().setVisible(true);
    }//GEN-LAST:event_btnAccueilActionPerformed

    private void cbEven1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEven1ActionPerformed
        // TODO add your handling code here:
        cbPart1.removeAllItems();
        int index;
        index=cbEven1.getSelectedIndex();
        lesParticipantsDeLEvenement = participant.findParticipantAEvenement((int)lesEvenements.get(index).getNum_even());
        if (lesParticipantsDeLEvenement.size()==0){
            cbPart1.addItem("Aucun participant");
        }
        else {
            for (Participant unParticipant : lesParticipantsDeLEvenement) {
                cbPart1.addItem(unParticipant.getNum_pers()+" "+unParticipant.getNom());
            }
        }
    }//GEN-LAST:event_cbEven1ActionPerformed

    private void btnRetirerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetirerActionPerformed
        // TODO add your handling code here:
        String str = (String)cbPart1.getSelectedItem().toString().substring(0,1);
        int num_pers = Integer.parseInt(str);
        int index2 = cbEven1.getSelectedIndex();
        inscrire.delete(new Inscrire(num_pers,lesEvenements.get(index2).getNum_even()));  
        cbPart1.removeAllItems();
        int index = cbEven1.getSelectedIndex();
        lesParticipantsDeLEvenement = participant.findParticipantAEvenement((int)lesEvenements.get(index).getNum_even());
        if (lesParticipantsDeLEvenement.size()==0){
            cbPart1.addItem("Aucun participant");
        }
        else {
            for (Participant unParticipant : lesParticipantsDeLEvenement) {
                cbPart1.addItem(unParticipant.getNum_pers()+" "+unParticipant.getNom());
            }
        }     
    }//GEN-LAST:event_btnRetirerActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccueil;
    private javax.swing.JButton btnAjout;
    private javax.swing.JButton btnRetirer;
    private javax.swing.JComboBox<String> cbEven;
    private javax.swing.JComboBox<String> cbEven1;
    private javax.swing.JComboBox<String> cbPart;
    private javax.swing.JComboBox<String> cbPart1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    // End of variables declaration//GEN-END:variables
}
