/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvven;

/**
 *
 * @author mat
 */
public class typeEvenement {
    private int type_event;
    private String nom_theme;
    
    public typeEvenement(){
        
    }
    
    public typeEvenement(String a){
        this.nom_theme = a;
    }
    
    public typeEvenement(int a, String b){
        this.type_event = a;
        this.nom_theme = b;
    }

    public int getType_event() {
        return type_event;
    }

    public void setType_event(int type_event) {
        this.type_event = type_event;
    }

    public String getNom_theme() {
        return nom_theme;
    }

    public void setNom_theme(String nom_theme) {
        this.nom_theme = nom_theme;
    }

    @Override
    public String toString() {
        return "typeEvenement{" + "type_event=" + type_event + ", nom_theme=" + nom_theme + '}';
    }
    
    
    
}
